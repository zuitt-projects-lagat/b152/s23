//Create:

db.users.insertOne({
    "firstName": "Spongebob",
    "lastName": "Squarepants",
    "email": "spongePants@gmail.com",
    "password": "iamready",
    "isAdmin": false
});

db.users.insertOne({
    "firstName": "Patrick",
    "lastName": "Star",
    "email": "rockStar@gmail.com",
    "password": "uhhhhh",
    "isAdmin": false
});


db.users.insertMany([
    {
        "firstName": "Squidward",
        "lastName": "Tentacles",
        "email": "squidward.tentacles@gmail.com",
        "password": "clarinet8",
        "isAdmin": false
    },
    {
        "firstName": "Sandy",
        "lastName": "Cheeks",
        "email": "texasbaby@gmail.com",
        "password": "yeehaw",
        "isAdmin": false
    },
    {
        "firstName": "Eugene",
        "lastName": "Krabs",
        "email": "krustykrab@gmail.com",
        "password": "money",
        "isAdmin": false
    }
]);



db.courses.insertMany([
    {
        "name": "Philosophy 1",
        "price": 5000,
        "isActive": false
    },
    {
        "name": "Politics 2",
        "price": 6500,
        "isActive": false
    },
    {
        "name": "Sociology",
        "price": 4800,
        "isActive": false
    }
]);



//Read:
db.getCollection('users').find({"isAdmin": false});



//Update:
db.users.updateOne({},{$set:{"isAdmin": true}});

db.courses.updateOne({"name": "Philosophy 1"},{$set:{"isActive": true}});



//Delete:
db.courses.deleteMany({"isActive": false});